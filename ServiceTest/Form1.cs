﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ReglasDeNegocio_CorregirBasculaService;

namespace ServiceTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReglasDeNegocio_CorregirBasculaService.CorregirBascula corregirBascula = new ReglasDeNegocio_CorregirBasculaService.CorregirBascula();

            corregirBascula.ScheduleService();
        }
    }
}
