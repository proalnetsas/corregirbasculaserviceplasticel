﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Threading;
using System.Configuration;
using System.Data.SqlClient;

namespace CorregirBascula
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            
            this.ScheduleService();
        }

        protected override void OnStop()
        {
           
            this.Schedular.Dispose();
        }

        private Timer Schedular;

        public void ScheduleService()
        {
            try
            {
                Schedular = new Timer(new TimerCallback(SchedularCallback));

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;

                //Get the Interval in Minutes from AppSettings.
                int intervalMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["IntervalMinutes"]);

                //Set the Scheduled Time by adding the Interval to Current Time.
                scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                if (DateTime.Now > scheduledTime)
                {
                    //If Scheduled Time is passed set Schedule for the next Interval.
                    scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                }


                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);

                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en CorregirBascula() " + ex.Message + ex.StackTrace);
            }
        }

        private void SchedularCallback(object e)
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["PlasticelDB"].ConnectionString;

                SqlDataAdapter adapter = new SqlDataAdapter();

                string sql = "SP_CORREGIR_ENTRADAEJECUCION_BASCULA";

                SqlConnection connection = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@HORASEJECUCION", SqlDbType.Int).Value = Convert.ToInt32(ConfigurationManager.AppSettings["HorasProgamadas"]);


                connection.Open();
                command.ExecuteNonQuery();

                connection.Close();

                this.ScheduleService();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al procesar en servicio corregir bascula: " + ex.Message + ex.StackTrace);
            }

        }

    }
}
